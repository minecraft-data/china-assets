// __multiversion__
// This shader is from the Gameface library modified to work in the
// Bedrock engine and heavily depends on Gameface's functionality, why
// some numbers and values seem arbitrary or "magic".

// To use centroid sampling we need to have version 300 es shaders, which requires changing:
// attribute to in
// varying to out when in vertex shaders or in when in fragment shaders
// defining an out vec4 FragColor and replacing uses of gl_FragColor with FragColor
// texture2D to texture
#if __VERSION__ >= 300

// version 300 code

#define varying in
#define texture2D texture
out vec4 FragColor;
#define gl_FragColor FragColor

#else

// version 100 code

#endif


#ifndef _UNIFORM_UI_GLOBAL_PIXEL_CONSTANTS_H
#define _UNIFORM_UI_GLOBAL_PIXEL_CONSTANTS_H

#ifndef _UNIFORM_MACRO_H
#define _UNIFORM_MACRO_H

#ifdef MCPE_PLATFORM_NX
// Unfortunately this macro does not work on old Amazon platforms #define BEGIN_UNIFORM_BLOCK(x) uniform x {
#define END_UNIFORM_BLOCK };
#define UNIFORM 
#else
// Unfortunately this macro does not work on old Amazon platforms #define BEGIN_UNIFORM_BLOCK(x) 
#define END_UNIFORM_BLOCK
#define UNIFORM uniform 
#endif

#if __VERSION__ >= 420
#define LAYOUT_BINDING(x) layout(binding = x)
#else
#define LAYOUT_BINDING(x) 
#endif

#endif


#ifdef MCPE_PLATFORM_NX
#extension GL_ARB_enhanced_layouts : enable
layout(binding = 4) uniform UIGlobalPixelConstants {
#endif
// BEGIN_UNIFORM_BLOCK(UIGlobalPixelConstants) - unfortunately this macro does not work on old Amazon platforms so using above 3 lines instead
UNIFORM vec2 VIEWPORT_SIZE;
END_UNIFORM_BLOCK

#endif


varying vec4 ColorOut;
varying vec4 AdditionalOut;
varying vec3 ScreenNormalPosition;

void main() {
#if defined(NO_DERIVATIVES)
	float alpha = 1.0;
	vec4 outColor = ColorOut;
	vec2 pixel_step = vec2( 1.0 / VIEWPORT_SIZE.x, 1.0 / VIEWPORT_SIZE.y);

	vec2 offset = (ScreenNormalPosition.xy - AdditionalOut.xy) / AdditionalOut.zw;
	float test = dot(offset, offset) - 1.0;
	vec2 newOffset = (ScreenNormalPosition.xy + vec2(pixel_step.x, 0) - AdditionalOut.xy) / (AdditionalOut.zw);
	vec2 dudx = newOffset - offset;
	newOffset = (ScreenNormalPosition.xy + vec2(0, pixel_step.y) - AdditionalOut.xy) / (AdditionalOut.zw);
	vec2 dudy = newOffset - offset;
	vec2 gradient = vec2(2.0 * offset.x * dudx.x + 2.0 * offset.y * dudx.y,
							2.0 * offset.x * dudy.x + 2.0 * offset.y * dudy.y);
	float grad_dot = max(dot(gradient, gradient), 1.0e-4);
	float invlen = inversesqrt(grad_dot);

	alpha = clamp(0.5 - test * invlen, 0.0, 1.0);

	gl_FragColor = outColor * alpha;
#else
	float alpha = 1.0;
	vec4 outColor = ColorOut;

	vec2 offset = (ScreenNormalPosition.xy - AdditionalOut.xy) / AdditionalOut.zw;
	float test = dot(offset, offset) - 1.0;
	vec2 dudx = dFdx(offset);
	vec2 dudy = dFdy(offset);
	vec2 gradient = vec2(2.0 * offset.x * dudx.x + 2.0 * offset.y * dudx.y,
							2.0 * offset.x * dudy.x + 2.0 * offset.y * dudy.y);
	float grad_dot = max(dot(gradient, gradient), 1.0e-4);
	float invlen = inversesqrt(grad_dot);

	alpha = clamp(0.5 - test * invlen, 0.0, 1.0);

	gl_FragColor = outColor * alpha;
#endif
}
