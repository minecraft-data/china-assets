### ui/settings_sections/general_section.json

- add "graphics_level" ui node by sutao release_20201116_1.21
- synchronizing  code from vanilla by gzhuabo release_20210420_1.23
- ignore "raytracing_toggle" by gzhuabo release_20210420_1.23
- ignore "upscaling_toggle" by gzhuabo release_20210420_1.23

### ui/hud_screen.json

- synchronizing  code from vanilla by xujiarong release_20210420_1.23

### ui/inventory_screen.json

- synchronizing  code from vanilla by xujiarong release_20210420_1.23

### ui/permission_screen.json

- synchronizing  code from vanilla by xujiarong release_20210420_1.23