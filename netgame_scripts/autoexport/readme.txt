auto_export.bat：导出Apollo mod sdk。注释修改方式如下：
（1）运营指令：修改“运营指令.md”。
（2）通信：修改“netgame/fakeCode”目录下文件。具体如下：
	client和game/lobby通信：修改“fakeClientGame.py”
	master和game/lobby通信：修改“fakeMasterGame.py”
	service和game/lobby通信：修改“fakeServiceGame.py”
	service和service/master通信：修改“fakeMasterService.py”
（3）事件：修改“serverEvent.py”文件。具体如下：
	master事件：修改“master/serverEvent.py”
	service事件：修改“service/serverEvent.py”
	lobby/game事件：修改“lobbyGame/serverEvent.py”

faker_export.bat：生成fake代码，提供给开发者，可以自动代码提示。